﻿Imports System.Reflection
Imports Bwl.Framework

Public Class LocalSettingsSequrity
    Implements Cf.CommonSecurity.ILocalSettings

    Private _passSeq As PasswordSetting
    Private _checkPassSeq As BooleanSetting

    Private _settingsStorage As SettingsStorage

    Public Sub New(settingsStorage As SettingsStorage)
        _settingsStorage = settingsStorage

        Dim sequritySettings = _settingsStorage.CreateChildStorage("sequritySettings", "Настройка доступа администратора")
        _checkPassSeq = sequritySettings.CreateBooleanSetting("CheckPass", True, "Запрашивать ли пароль")
        _passSeq = sequritySettings.CreatePasswordSetting("MainPass", "", "Пароль настроек")
        If String.IsNullOrEmpty(_passSeq.Pass) Then
            _passSeq.Pass = "58963705"
        End If
    End Sub

    Public ReadOnly Property SettingsStorage As SettingsStorage
        Get
            Return _settingsStorage
        End Get
    End Property

    Public Property CheckPass As Boolean Implements CommonSecurity.ILocalSettings.CheckPass
        Get
            Return _checkPassSeq.Value
        End Get
        Set(value As Boolean)
            _checkPassSeq.Value = value
        End Set
    End Property

    Public Property Pass As String Implements CommonSecurity.ILocalSettings.Pass
        Get
            Return _passSeq.Pass
        End Get
        Set(value As String)
            _passSeq.Pass = value
        End Set
    End Property
End Class


''' <summary> Атрибут для настроек </summary>
<AttributeUsageAttribute(AttributeTargets.Field Or AttributeTargets.Property)>
Public Class SettingAttribute
    Inherits Attribute
End Class

''' <summary> Атрибут для объекта, который также содержит настройки</summary>
<AttributeUsageAttribute(AttributeTargets.Field Or AttributeTargets.Property)>
Public Class ObjWithSettingAttribute
    Inherits Attribute

    Public ReadOnly Property Name As String

    ''' <summary>В конструкторе можно указать имя настройки, иначе имя будет взято из имени поля в классе</summary>
    Public Sub New(Optional name As String = "")
        Me.Name = name
    End Sub
End Class


Public Class SettingsAttributeReader
    Public Shared Sub ReadRecursive(obj As Object, objName As String, settings As SettingsStorage)
        If obj IsNot Nothing Then
            Dim objSettings = settings.CreateChildStorage(objName)

            Dim objType = obj.GetType
            If (objType.GetInterface(GetType(IEnumerable).Name) IsNot Nothing) Then
                'просматриваем массивы и списки
                If obj IsNot Nothing Then
                    Dim list = CType(obj, IEnumerable)
                    Dim i = 0
                    For Each listItem In list
                        If listItem IsNot Nothing Then
                            ReadRecursive(listItem, objName + "_" + i.ToString, objSettings)
                        End If
                        i += 1
                    Next
                End If
            Else
                'просматриваем объекты
                Dim members = obj.GetType.GetMembers()
                Dim fields = members.Where(Function(m)
                                               Return m.MemberType = Reflection.MemberTypes.Property Or m.MemberType = Reflection.MemberTypes.Field
                                           End Function)
                If fields IsNot Nothing Then
                    For Each item In fields.ToArray
                        If item.MemberType = Reflection.MemberTypes.Property Or item.MemberType = Reflection.MemberTypes.Field Then
                            If item.GetCustomAttributes(GetType(SettingAttribute), True).Length > 0 Then
                                Dim setAdapter = New SettingsAdapter(obj, item, objSettings)
                            End If
                            If item.GetCustomAttributes(GetType(ObjWithSettingAttribute), True).Length > 0 Then
                                Dim subObj = GetMemberValue(item.Name, obj)

                                ReadRecursive(subObj, item.Name, objSettings)
                            End If
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Shared Sub SetMemberValue(fieldPath As String, obj As Object, value As Object)
        Dim fieldParts = fieldPath.Split("."c)

        If fieldParts.Length = 1 Then
            Dim members = obj.GetType.GetMember(fieldParts(0))
            If members.Length > 0 Then
                If members(0).MemberType = MemberTypes.Property Then
                    DirectCast(members(0), PropertyInfo).SetValue(obj, value, Nothing)
                ElseIf members(0).MemberType = MemberTypes.Field Then
                    DirectCast(members(0), FieldInfo).SetValue(obj, value)
                End If
            Else
                Throw New Exception("Member not found: " + fieldParts(0))
            End If
        Else
            Dim curNameMass = fieldParts(0).Split(";"c)
            Dim current = curNameMass(0)
            Dim members = obj.GetType.GetMember(current)

            Dim path = fieldParts(1)
            For i = 2 To fieldParts.Length - 1
                path += "." + fieldParts(i)
            Next

            If members.Any Then
                For Each member In members
                    If member.MemberType = MemberTypes.Property Then
                        obj = DirectCast(member, PropertyInfo).GetValue(obj, Nothing)
                        If obj Is Nothing Then Throw New Exception("Value is nothing not found: " + member.Name)
                        If curNameMass.Length = 2 Then
                            Dim iEnum = CType(obj, IEnumerable(Of Object))
                            obj = iEnum.ElementAt(Convert.ToInt32(curNameMass(1)))
                        End If
                        SetMemberValue(path, obj, value)
                    End If
                    If member.MemberType = MemberTypes.Field Then
                        obj = DirectCast(member, FieldInfo).GetValue(obj)
                        If curNameMass.Length = 2 Then
                            Dim iEnum = CType(obj, IEnumerable(Of Object))
                            obj = iEnum.ElementAt(Convert.ToInt32(curNameMass(1)))
                        End If
                        If obj Is Nothing Then Throw New Exception("Value is nothing not found: " + member.Name)
                        SetMemberValue(path, obj, value)
                    End If
                Next
            Else
                Throw New Exception("Member not found: " + current)
            End If
        End If
    End Sub

    Private Shared Function GetMemberValue(fieldPath As String, obj As Object) As Object
        Dim fieldParts = fieldPath.Split("."c)

        If fieldParts.Length = 1 Then
            Dim members = obj.GetType.GetMember(fieldParts(0))
            If members.Length > 0 Then
                If members(0).MemberType = MemberTypes.Property Then
                    Dim value = DirectCast(members(0), PropertyInfo).GetValue(obj, Nothing)
                    Return value
                End If
                If members(0).MemberType = MemberTypes.Field Then
                    Dim value = DirectCast(members(0), FieldInfo).GetValue(obj)
                    Return value
                End If
                Throw New Exception
            End If
            Throw New Exception("Member not found: " + fieldParts(0))
        Else
            Dim current = fieldParts(0)
            Dim members = obj.GetType.GetMember(current)

            Dim path = fieldParts(1)
            For i = 2 To fieldParts.Length - 1
                path += "." + fieldParts(i)
            Next

            For Each member In members
                If member.MemberType = MemberTypes.Property Then
                    Dim value = DirectCast(member, PropertyInfo).GetValue(obj, Nothing)
                    If value Is Nothing Then Throw New Exception("Value is nothing not found: " + member.Name)
                    Dim nested = GetMemberValue(path, value)
                    Return nested
                End If
                If member.MemberType = MemberTypes.Field Then
                    Dim value = DirectCast(member, FieldInfo).GetValue(obj)
                    Dim nested = GetMemberValue(path, value)
                    Return nested
                End If
            Next
            Throw New Exception("Member not found: " + current)
        End If
    End Function

End Class


Public Class SettingsAdapter
    Private _obj As Object
    Private _memberInfo As MemberInfo
    Private _storage As SettingsStorage
    Private _setting As Object
    Private _timer As Threading.Timer

    Public Sub New(obj As Object, memberInfo As MemberInfo, settings As SettingsStorage)
        _obj = obj
        _memberInfo = memberInfo
        _storage = settings
        Dim itemType = _memberInfo.DeclaringType

        Select Case itemType
            Case GetType(Integer)
                Dim s = settings.CreateIntegerSetting(_memberInfo.Name, 0)
                AddHandler s.ValueChanged, AddressOf OnValueChanged
                _setting = s
            Case GetType(String)
                Dim s = settings.CreateStringSetting(_memberInfo.Name, "")
                AddHandler s.ValueChanged, AddressOf OnValueChanged
                _setting = s
        End Select

        OnValueChanged()

        _timer = New Threading.Timer(AddressOf OnValueChanged)
        _timer.Change(0, Threading.Timeout.Infinite)
    End Sub

    Private Sub RefreshThread()
        Try
            Dim value = GetMemberValue(_memberInfo.Name, _obj)
            _setting.Value = value
        Catch ex As Exception
            '...
        End Try
        _timer.Change(New TimeSpan(0, 0, 30), New TimeSpan(-1))
    End Sub

    Private Sub OnValueChanged()
        SetMemberValue(_memberInfo.Name, _obj, _setting.Value)
    End Sub

    Shared Sub SetMemberValue(fieldPath As String, obj As Object, value As Object)
        Dim fieldParts = fieldPath.Split("."c)

        If fieldParts.Length = 1 Then
            Dim members = obj.GetType.GetMember(fieldParts(0))
            If members.Length > 0 Then
                If members(0).MemberType = MemberTypes.Property Then
                    DirectCast(members(0), PropertyInfo).SetValue(obj, value, Nothing)
                ElseIf members(0).MemberType = MemberTypes.Field Then
                    DirectCast(members(0), FieldInfo).SetValue(obj, value)
                End If
            Else
                Throw New Exception("Member not found: " + fieldParts(0))
            End If
        Else
            Dim curNameMass = fieldParts(0).Split(";"c)
            Dim current = curNameMass(0)
            Dim members = obj.GetType.GetMember(current)

            Dim path = fieldParts(1)
            For i = 2 To fieldParts.Length - 1
                path += "." + fieldParts(i)
            Next

            If members.Any Then
                For Each member In members
                    If member.MemberType = MemberTypes.Property Then
                        obj = DirectCast(member, PropertyInfo).GetValue(obj, Nothing)
                        If obj Is Nothing Then Throw New Exception("Value is nothing not found: " + member.Name)
                        If curNameMass.Length = 2 Then
                            Dim iEnum = CType(obj, IEnumerable(Of Object))
                            obj = iEnum.ElementAt(Convert.ToInt32(curNameMass(1)))
                        End If
                        SetMemberValue(path, obj, value)
                    End If
                    If member.MemberType = MemberTypes.Field Then
                        obj = DirectCast(member, FieldInfo).GetValue(obj)
                        If curNameMass.Length = 2 Then
                            Dim iEnum = CType(obj, IEnumerable(Of Object))
                            obj = iEnum.ElementAt(Convert.ToInt32(curNameMass(1)))
                        End If
                        If obj Is Nothing Then Throw New Exception("Value is nothing not found: " + member.Name)
                        SetMemberValue(path, obj, value)
                    End If
                Next
            Else
                Throw New Exception("Member not found: " + current)
            End If
        End If
    End Sub

    Private Shared Function GetMemberValue(fieldPath As String, obj As Object) As Object
        Dim fieldParts = fieldPath.Split("."c)

        If fieldParts.Length = 1 Then
            Dim members = obj.GetType.GetMember(fieldParts(0))
            If members.Length > 0 Then
                If members(0).MemberType = MemberTypes.Property Then
                    Dim value = DirectCast(members(0), PropertyInfo).GetValue(obj, Nothing)
                    Return value
                End If
                If members(0).MemberType = MemberTypes.Field Then
                    Dim value = DirectCast(members(0), FieldInfo).GetValue(obj)
                    Return value
                End If
                Throw New Exception
            End If
            Throw New Exception("Member not found: " + fieldParts(0))
        Else
            Dim current = fieldParts(0)
            Dim members = obj.GetType.GetMember(current)

            Dim path = fieldParts(1)
            For i = 2 To fieldParts.Length - 1
                path += "." + fieldParts(i)
            Next

            For Each member In members
                If member.MemberType = MemberTypes.Property Then
                    Dim value = DirectCast(member, PropertyInfo).GetValue(obj, Nothing)
                    If value Is Nothing Then Throw New Exception("Value is nothing not found: " + member.Name)
                    Dim nested = GetMemberValue(path, value)
                    Return nested
                End If
                If member.MemberType = MemberTypes.Field Then
                    Dim value = DirectCast(member, FieldInfo).GetValue(obj)
                    Dim nested = GetMemberValue(path, value)
                    Return nested
                End If
            Next
            Throw New Exception("Member not found: " + current)
        End If
    End Function
End Class