﻿Imports Cf.CommonSecurity

Public Class Form1

    Private _security As ISequrity

    Public Sub New()

        ' Этот вызов является обязательным для конструктора.
        InitializeComponent()

        ' Добавить код инициализации после вызова InitializeComponent().
        Dim localLogger = New LocalLogger(AppBase.RootLogger)
        Dim seqSettings = New LocalSettingsSequrity(AppBase.RootStorage)
        _security = New Cf.CommonSecurity.LoginForm_Pass(localLogger, seqSettings)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If _security.CheckPassGUI("Выполнение действия 1", Me) Then
            Action1()
        End If
    End Sub

    Public Sub Action1()

    End Sub
End Class
