﻿Imports Bwl.Framework

Public Class LocalLogger
    Implements Cf.CommonSecurity.ILogger

    Private ReadOnly _logger As Logger

    Public Sub New(logger As Logger)
        _logger = logger
    End Sub

    Public ReadOnly Property Logger As Logger
        Get
            Return _logger
        End Get
    End Property

    Public Sub AddError(msg As String)
        _logger.AddError(msg)
    End Sub

    Public Sub AddWarning(msg As String) Implements CommonSecurity.ILogger.AddWarning
        _logger.AddWarning(msg)
    End Sub

End Class

