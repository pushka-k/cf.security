﻿	Private _passSet As PasswordSetting
	Private _checkPass As BooleanSetting
	_checkPass = SettingsStorage.CreateBooleanSetting("CheckPass", True, "Запрашивать ли пароль")
	_passSet = SettingsStorage.CreatePasswordSetting("MainPass", "", "Пароль настроек")
	If String.IsNullOrEmpty(_passSet.Pass) Then
		_passSet.Pass = "admin"
	End If