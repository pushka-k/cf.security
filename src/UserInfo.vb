﻿Imports Bwl.Framework

Public Class UserInfo
    Private _name As StringSetting
    Private _pass As PasswordSetting
    Private _actions As PasswordSetting

    Public Sub New(settingsForUser As SettingsStorage)
        _name = settingsForUser.CreateStringSetting("Name", "Имя пользователя")
        _pass = settingsForUser.CreatePasswordSetting("Pass", "")
        _actions = settingsForUser.CreatePasswordSetting("Actions", "")
    End Sub

End Class

Public Class UsersSecurity
    Private _usersCount As IntegerSetting
    Private ReadOnly _users As New List(Of UserInfo)
    Private _currentUser As UserInfo
End Class
