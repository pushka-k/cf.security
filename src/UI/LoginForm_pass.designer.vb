﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoginForm_Pass
	Inherits System.Windows.Forms.Form

	'Форма переопределяет dispose для очистки списка компонентов.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Является обязательной для конструктора форм Windows Forms
	Private components As System.ComponentModel.IContainer

	'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
	'Для ее изменения используйте конструктор форм Windows Form.  
	'Не изменяйте ее в редакторе исходного кода.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoginForm_Pass))
        Me._btnOk = New System.Windows.Forms.Button()
        Me._btnCancel = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me._txtPass = New System.Windows.Forms.TextBox()
        Me._VirtKeyBtnComment = New Cf.VirtualKeyboards.VirtKeyBtn(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me._actionText = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        '_btnOk
        '
        Me._btnOk.DialogResult = System.Windows.Forms.DialogResult.OK
        Me._btnOk.Location = New System.Drawing.Point(12, 73)
        Me._btnOk.Name = "_btnOk"
        Me._btnOk.Size = New System.Drawing.Size(75, 23)
        Me._btnOk.TabIndex = 0
        Me._btnOk.Text = "Принять"
        Me._btnOk.UseVisualStyleBackColor = True
        '
        '_btnCancel
        '
        Me._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._btnCancel.Location = New System.Drawing.Point(93, 73)
        Me._btnCancel.Name = "_btnCancel"
        Me._btnCancel.Size = New System.Drawing.Size(75, 23)
        Me._btnCancel.TabIndex = 1
        Me._btnCancel.Text = "Отмена"
        Me._btnCancel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Введите пароль:"
        '
        '_txtPass
        '
        Me._txtPass.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._txtPass.Location = New System.Drawing.Point(12, 47)
        Me._txtPass.Name = "_txtPass"
        Me._txtPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me._txtPass.Size = New System.Drawing.Size(157, 20)
        Me._txtPass.TabIndex = 3
        Me._txtPass.Text = "58963705"
        '
        '_VirtKeyBtnComment
        '
        Me._VirtKeyBtnComment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._VirtKeyBtnComment.BackColor = System.Drawing.SystemColors.Control
        Me._VirtKeyBtnComment.FocusControl = Me._txtPass
        Me._VirtKeyBtnComment.Image = CType(resources.GetObject("_VirtKeyBtnComment.Image"), System.Drawing.Image)
        Me._VirtKeyBtnComment.Location = New System.Drawing.Point(174, 47)
        Me._VirtKeyBtnComment.Name = "_VirtKeyBtnComment"
        Me._VirtKeyBtnComment.PassMode = True
        Me._VirtKeyBtnComment.Size = New System.Drawing.Size(52, 49)
        Me._VirtKeyBtnComment.TabIndex = 37
        Me._VirtKeyBtnComment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._VirtKeyBtnComment.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "Действие:"
        '
        '_actionText
        '
        Me._actionText.AutoSize = True
        Me._actionText.Location = New System.Drawing.Point(75, 9)
        Me._actionText.Name = "_actionText"
        Me._actionText.Size = New System.Drawing.Size(110, 13)
        Me._actionText.TabIndex = 39
        Me._actionText.Text = "Настройка системы"
        '
        'LoginForm_Pass
        '
        Me.AcceptButton = Me._btnOk
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me._btnCancel
        Me.ClientSize = New System.Drawing.Size(234, 103)
        Me.ControlBox = False
        Me.Controls.Add(Me._actionText)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me._VirtKeyBtnComment)
        Me.Controls.Add(Me._txtPass)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me._btnCancel)
        Me.Controls.Add(Me._btnOk)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "LoginForm_Pass"
        Me.Text = "Запрос доступа"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents _btnOk As System.Windows.Forms.Button
	Friend WithEvents _btnCancel As System.Windows.Forms.Button
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents _txtPass As System.Windows.Forms.TextBox
	Friend WithEvents _VirtKeyBtnComment As VirtualKeyboards.VirtKeyBtn
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents _actionText As Windows.Forms.Label
End Class
