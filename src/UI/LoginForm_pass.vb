﻿Imports System.Windows.Forms

Public Class LoginForm_Pass
	Implements ISequrity
	Private _logger As ILogger
	Private _localSettings As ILocalSettings

	Public Sub New(logger As ILogger, localSettings As ILocalSettings)
		InitializeComponent()
		_localSettings = localSettings
		_logger = logger
		Visible = False
	End Sub

    Public Function CheckPassGUI(action As String, parentForm As IWin32Window, Optional passType As PassType = PassType.Admin) As Boolean Implements ISequrity.CheckPassGUI
        Dim res = True
        _actionText.Text = action
        If _localSettings.CheckPass Then
            res = False
            _txtPass.Focus()
            _txtPass.Text = String.Empty
            If (ShowDialog(parentForm) = Windows.Forms.DialogResult.OK) Then
                If (_txtPass.Text = _localSettings.Pass) Then
                    res = True
                    _logger.AddWarning("Sequrity.GoodPass _ Action:" + action)
                Else
                    _logger.AddWarning("Sequrity.BadPass _ Action:" + action)
                End If
            End If
        Else
            _logger.AddWarning("Sequrity.CheckPassGUI проверка пароля отключена _ Action:" + action)
        End If
        Return res
    End Function
End Class